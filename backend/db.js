const mysql =require('mysql2')

const pool =mysql.createPool({
    host:'mysqldb',
    user:'root',
    password:'manager',
    database:'data',
    port: 3306,
    waitForConnections: true,
    connectionLimit:10,
    queueLimit:0
    
})
module.exports= pool