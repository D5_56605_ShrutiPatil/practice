const express= require('express')
const utils= require('../utils')
const db= require('../db')
const router= express.Router()

router.get('/getproduct', (request, response)=>{
    const query= `select * from product`
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.post('/addproduct', (request, response)=>{
    const {id, title, description, price}=request.body
    const query= `insert into product 
    values
    ('${id}', '${title}', '${description}', '${price}')`
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})
router.put('/updateproduct', (request, response)=>{
    const {id, price}=request.body
    const query= `update product set price='${price}' where id='${id}'`
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

router.delete('/deleteproduct', (request, response)=>{
    const {id}=request.body
    const query= `delete from product where id='${id}'`
    db.execute(query, (error, result)=>{
        response.send(utils.createResult(error, result))
    })
})

module.exports= router