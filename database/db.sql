create table product
(
    id integer,
    title varchar(50),
    description varchar(200),
    price double
);

insert into product values(1, "samsung", "mobile", 20000);
insert into product values(1, "motorola", "TV", 80000);
